/*
       #include <unistd.h>

       int chdir(const char *path);
         作用：修改进程的工作目录
         比如在  /home/nowcoder
   启动了一个可执行程序a.out,进程的工作目录就是/home/nowcoder 参数：
                        path:需要修改的工作目录
       #include <unistd.h>

       char *getcwd(char *buf, size_t size);
      作用：获取当前的工作目录
      参数：
      -buf:存储的路径，指向的是一个数组(传出参数)
      -size:数组的大小
      返回值：
            返回的指向的一块内存，这个数据就是第一个参数
*/
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  //获取当前的工作目录
  char buf[128];
  getcwd(buf, sizeof(buf));
  printf("当前的工作目录%s\n", buf);
  //修改工作目录
  int ret = chdir("/home/huang/Linuxnetwork/day01/fileIO1");
  if (ret == -1) {
    printf("修改失败\n");
    return -1;
  } else {
    printf("修改成功\n");
  }
  int fd = open("chdir.txt", O_CREAT | O_RDWR, 0777);
  if (fd == -1) {
    perror("open");
    return -1;
  }
  getcwd(buf, sizeof(buf));
  printf("当前的工作目录%s\n", buf);
  return 0;
}