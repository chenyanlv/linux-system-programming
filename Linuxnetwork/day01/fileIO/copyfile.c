/*
 #include <unistd.h>
ssize_t read(int fd, void *buf, size_t count);
    参数:
        -fd:文件描述符，open得到的，通过这个文件描述符操作某个文件
        -buf:需要读取数据存放的地方，数组的地址
        -count:指定的数组的大小
    返回值:
         -成功：
           >0:返回实际的读取到的字节数
           =0：文件已经读取完了
    -1是调用失败
    #include<unistd.h>
    ssize_t write(int fd,const void *buf,size_t count);
    参数：
       -fd:文件描述符，open得到的，通过这个文件描述符操作某个文件
       -buf:要往磁盘写入的数据，数据
       -count:要写的数据的实际的大小
    返回值：
       成功：实际写入的字节数
       失败：返回-1,并设置error
*/
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  // 1. 通过open 打开文件
  int srcfd = open("test.txt", O_RDONLY);
  if (srcfd == -1) {
    perror("open");
  }
  // 2.  创建一个新的文件
  int destfd = open("copy.txt", O_WRONLY | O_CREAT, 0777);
  if (destfd == -1) {
    perror("create");
  }
  // 3.  频繁的读写文件
  char buf[1024] = {0};
  int len = 0;
  while ((len = read(srcfd, buf, sizeof(buf)))) {
    write(destfd, buf, len);
  }
  close(destfd);
  close(srcfd);
  // 4. 关闭文件
}