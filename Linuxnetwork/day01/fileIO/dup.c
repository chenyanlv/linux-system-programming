/*
       #include <unistd.h>
       //复制一个新的文件描述符 ，指向同一个文件的
       int dup(int oldfd);
       //传入一个旧的 fd  返回一个最小的未被使用的fd
       返回值：
              成功返回一个最小的未被使用的fd作为新的未被拷贝的文件描述符
              失败返回-1

*/
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  int fd = open("c.txt", O_RDWR | O_CREAT, 0664);
  int fd1 = dup(fd);
  if (fd == -1) {
    perror("dup");
    return -1;
  }
  printf("fd:%d,fd1:%d", fd, fd1);
  close(fd);
  char *ptr = "helloworld";
  int ret = write(fd1, ptr, strlen(ptr));
  if (ret == -1) {
    perror("write");
  }
}