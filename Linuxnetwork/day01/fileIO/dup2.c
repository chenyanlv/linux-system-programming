/*
     #include <unistd.h>
     int dup2(int oldfd, int newfd);
           作用：重定向文件描述符
           oldfd 指向a.txt    newfd 指向 b.txt
           调用函数 成功后:newfd和b.txt做close,newfd指向了a.txt
                    oldfd必须是有效的文件描述符
                    如果值相同，那就相当于什么都没做
*/
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  int fd = open("a.txt", O_RDWR);
  int fd1 = open("b.txt", O_RDWR);
  int fd2 = dup2(fd, fd1);  //关闭 fd1  在获取fd1的值进行重定向到fd
  printf("fd:%d,fd1:%d,fd2:%d", fd, fd1, fd2);
  //这时候 fd2指向了  a.txt
  close(fd);
  close(fd2);
}