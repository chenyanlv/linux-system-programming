/*
       #include <fcntl.h>
       #include <unistd.h>
复制文件描述符
设置获取文件的状态标志
        int fcntl(int fd, int cmd, ... arg  );
        参数：
        -fd:需要操作的文件描述符
        -cmp:代表打算操作的指令
           -F_DUPFD:复制文件描述符，复制第一参数fd，得到第一个新的文件描述符
           int ret=fcntl(fd,F_DUPFD)
           -F_GETFL:获取文件的状态flag
           获取的flag和通过open函数传递的东西相同
           -F_SETFL:设置文件描述符文件状态flag
           必选项:O_RDONLY,O_WRONLY,ORDWR
           可选项:O_CREATE,O_APPEND，O_NOBLOCK
             O_APPEND 表示追加数据
             O_NOBLOCK  设置成非阻塞

         阻塞和非阻塞：描述的是函数调用的行为。
*/
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
int main() {
  //
  //  int fd=open("a.txt",O_RDONLY);

  //   int ret=fcntl(fd,F_DUPFD);
  //修改或者获取文件状态flag
  int fd = open("a.txt", O_RDWR);
  int flag = fcntl(fd, F_GETFL);
  flag |= O_APPEND;  // 保留之前的flag并且追加O_APPEND
  int ret = fcntl(fd, F_SETFL, flag);
  char *buf = "你好";
  write(fd, buf, strlen(buf));
  close(fd);
  return 0;
}