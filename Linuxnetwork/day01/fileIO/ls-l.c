#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
//模拟实现 ls -l 指令
//-rw-rw-r-- 1 huang huang   793 十月   23 18:46 test.txt
int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("%s filename\n", argv[0]);
  }
  struct stat ds;

  //通过 stat函数获取用户传入的文件的信息
  int ret = stat(argv[1], &ds);
  if (ret == -1) {
    perro("stat");
  }
  char perms[11] = {0};
  //文件所有者  pwd.h。
  char *fileuser = getpwuid(ds.st_uid)->pw_name;
  //文件所在组  grp.h.
  char *fileGrp = getgrgid(ds.st_gid)->gr_gid;

  //获取修改时间
  long int filesize = ds.st_size;
  char *time = ctime(&ds.st_mtim);
}