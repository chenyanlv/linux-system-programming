/*
       标准c库
       #include <stdio.h>
       int fseek(FILE *stream, long offset, int whence);

       Linux系统函数
       #include <sys/types.h>
       #include <unistd.h>

       off_t lseek(int fd, off_t offset, int whence);
          参数:
          -fd:文件描述符，通过open得到的，通过这个fd操作得到
          -offset:偏移量
          -whence:
             SEEK_SET
                 设置文件指针的偏移量
             SEEK_CUR
                  设置偏移量：当前位置+第二个参数offset
             SEEK_END
                  设置偏移量：文件大小+第二个参数offset
             返回文件指针的位置
    作用：
        1.移动文件指针到头文件
        lseek(fd,0,SEEK_SET);
        2.获取当前文件指针的位置
        lseek(fd,0,SEEK_CUR);
        3.获取文件的长度
        lseek(fd,0,SEEK_END);
        4.拓展文件的长度，当前文件10b,110b,增加100个字节
   还要写入一个空数据才能拓展 用途：提前拓展文件的大小   在下载文件的场景中使用
        lseek(fd,100,SEEK_END);
*/