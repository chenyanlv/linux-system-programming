
/*
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
//打开一个已经存在的文件
//  int open(const char *pathname, int flags);
     参数：
        pathname:要打开的文件路径
            flags:O_RDONLY, O_WRONLY, O_RDWR.   这三个设置是互斥的
        返回值:返回一个新的文件描述符  失败一般是返回-1
        errno:属于Linux系统函数库，库里面的一个全局变量，记录的是最近的错误号
        #include<stdio.h>
        void  perror(const char *s)  作用：打印erron对应的错误描述
        S参数：用户描述，比如hello  ,最终输出的内容是   hello:xxx/

     //创建一个新的文件
//  int open(const char *pathname, int flags, mode_t mode);
   必选  flags:O_RDONLY, O_WRONLY, O_RDWR.   这三个设置是互斥的
   可选项：O_CREAT文件不存在，创建文件    等等
    mode:文件的模式
    最终权限   mode&~umask
    实例  0777  二进制   做运算
    umask  抹去一些权限

flags占4个字节  占32个比特位  每一位就是一个标志位
|运算做标记位保留

      //关闭文件描述符
      #include <unistd.h>
      int close(int fd);
*/
#include <fcntl.h>
#include <stdio.h>  //perror
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>  // close
int main() {
  // int fd = open("a.txt", O_RDONLY);
  // if (fd == -1) {
  //   perror("open");
  // }
  // close(fd);
  int fd = open("a.txt", O_RDWR | O_CREAT, 0777);
  if (fd == -1) {
    perror("open");
  }
  close(fd);
}