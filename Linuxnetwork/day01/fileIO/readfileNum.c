/*
       #include <dirent.h>
       #include <sys/types.h>

       DIR *opendir(const char *name);
            -参数：
                -name:需要打开的目录的名称
            -返回值：
                DIR* 类型  ，理解为目录流信息
              错误值返回NULL


       //读取目录中的数据
       #include <dirent.h>
       struct dirent *readdir(DIR *dirp);
            -参数：dirp是opendir返回的结果
            -返回值：
            struct dirent,代表读取到的文件信息
              读取到末尾或者失败了是，返回NULL


      //关闭目录
       #include <dirent.h>
       #include <sys/types.h>

       int closedir(DIR *dirp);

*/
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
//读取某个目录下的普通文件的个数
int getfileNum(const char *path);
int main(int argc, const char *argv[]) {
  if (argc < 2) {
    printf("%s path\n", argv[0]);
    return -1;
  }
  int num = getfileNum(argv[1]);
  printf("普通文件的个数：%d\n", num);
}
int getfileNum(const char *path) {
  // 1打开目录
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("opendir");
    exit(0);
  }
  int total = 0;
  struct dirent *ptr;
  while ((ptr = readdir(dir)) != NULL) {
    char *dname = ptr->d_name;
    if ((strcmp(dname, ".") == 0) || strcmp(dname, "..") == 0) {
      continue;
    }
    //判断是否是普通文件
    if (ptr->d_type == DT_DIR) {
      //目录继续读取
      char newpath[256];
      sprintf(newpath, "%s/%s", path, dname);
      total += getfileNum(newpath);
    }
    if (ptr->d_type == DT_REG) {
      //普通文件total++
      ++total;
    }
  }
  closedir(dir);
  return total;
}