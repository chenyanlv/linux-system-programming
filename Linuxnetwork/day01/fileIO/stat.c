/*
    #include <sys/stat.h>
       #include <sys/types.h>
    #include <unistd.h>
stat结构体：
struct stat {
    dev_t         st_dev;       //文件的设备编号
    ino_t          st_ino;       //节点
    mode_t      st_mode;      //文件的类型和存取的权限
    nlink_t        st_nlink;     //连到该文件的硬连接数目，刚建立的文件值为1
    uid_t          st_uid;       //用户ID
    gid_t          st_gid;       //组ID
    dev_t         st_rdev;      //(设备类型)若此文件为设备文件，则为其设备编号
    off_t          st_size;      //文件字节数(文件大小)
    unsigned long st_blksize;   //块大小(文件系统的I/O 缓冲区大小)
    unsigned long st_blocks;    //块数
    time_t        st_atime;     //最后一次访问时间
    time_t        st_mtime;     //最后一次修改时间
    time_t        st_ctime;     //最后一次改变时间(指属性)
};
    int stat(const char *pathname, struct stat *statbuf);
    作用：获取一个文件相关的一些信息
    参数：
        -pathname :操作的文件的路径
        -statbuf:结构变量，传出参数，用于保存获取到的文件的信息
    返回值：
        成功：返回0
        失败：返回-1 设置erron
    st_mode变量
    文件类型  特殊权限位   User   Group  Others
     0 0 0 0   0  0  0    000    000     000
文件类型：一个或多个标志位  一般和 1111 00000000000做完&操作再和对应文件类型
权限：  和对应权限的erjinz

ln -s a.txt b.txt
stat 的话会获取  b.txt指向的a.txt的文件信息
lstat 获取 软链接b.txt 的文件信息
    int fstat(int fd, struct stat *statbuf);
    int lstat(const char *pathname, struct stat *statbuf);


*/
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  struct stat statbuf;
  int ret = stat("a.txt", &statbuf);
  if (ret == -1) {
    perror("stat");
    return -1;
  }
  printf("size: %ld\n", statbuf.st_size);
}