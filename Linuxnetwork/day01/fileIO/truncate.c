/*
       #include <sys/types.h>
       #include <unistd.h>

       int truncate(const char *path, off_t length);
          作用：缩减或者拓展文件的尺寸至指定的大小
          参数：
          -path:需要修改的文件的了路径
          -length:需要最终文件变成的大小
          返回值和之前的一样  成功0   失败  -1
*/
#include <sys/types.h>
#include <unistd.h>

int main() { truncate("a.txt", 20); }