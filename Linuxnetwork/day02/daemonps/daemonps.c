/*
写一个守护进程,每隔2s获取一下系统时间，
将这个时间写入到磁盘文件中
*/
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
void work() {
  time_t tm = time(NULL);
  struct tm *my_tm = localtime(&tm);
  char buf[1024];
  static int i = 0;
  sprintf(buf, "%d_%02d_%02d %02d:%02d %02ds\n", my_tm->tm_year + 1900,
          my_tm->tm_mon + 1, my_tm->tm_mday, my_tm->tm_hour, my_tm->tm_min,
          my_tm->tm_sec);
  int fd = open("log2.txt", O_RDWR | O_CREAT | O_APPEND, 0664);
  write(fd, buf, strlen(buf));
  close(fd);
}
int main() {
  //创建子进程，退出父进程/)

  pid_t pid = fork();
  if (pid > 0) {
    exit(0);
  }
  //将子进程重新创建一个会话
  setsid();
  //设置掩码
  umask(022);
  //更改
  chdir("/home/huang");
  //
  int fd = open("dev/null", O_RDWR);
  dup2(fd, STDIN_FILENO);
  dup2(fd, STDOUT_FILENO);
  dup2(fd, STDERR_FILENO);
  //业务逻辑
  //捕捉信号
  struct sigaction act;
  act.sa_flags = 0;
  act.sa_handler = work;
  sigemptyset(&act.sa_mask);
  sigaction(SIGALRM, &act, NULL);

  //创建定时器
  struct itimerval val;
  val.it_value.tv_sec = 2;
  val.it_value.tv_usec = 0;
  val.it_interval.tv_sec = 2;
  val.it_interval.tv_usec = 0;
  setitimer(ITIMER_REAL, &val, NULL);
  while (1) {
  }
}