//文件execle.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//函数原型：int execle(const char *path, const char *arg,..., char * const
// envp[]);

char *env_init[] = {"AA=aa", "BB=bb", NULL};
int main(void) {
  printf("before execle****\n");
  if (execle("/bin/echoenv", "echoenv", NULL, env_init) == -1) {
    printf("execle failed!\n");
  }
  printf("after execle*****\n");
  return 0;
}