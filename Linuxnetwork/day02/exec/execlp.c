/*
       #include <unistd.h>


     int execlp(const char *file, const char *arg, ...);
          -回到环境变量下去找，如果找不到就执行不成功。
         参数：
            -file：需要指定执行的文件的文件名
                 a.out    ps
            -arg:是执行可执行文件所需要的参数列表
            第一个参数一般没有什么，为了方便，一般写的是执行的程序的名称
            从第二个参数开始往后，就是程序执行所需要的参数列表
            参数后需要以NULL结束(哨兵)
            -返回值
               只有当调用失败，才有返回值，返回-1，并且设置errno,并且原进程中的代码会继续执行
               如果调用成功没有返回值
   调用成功被原用户区的数据替换，就没有返回值了。

       int execv(const char *pathname, char *const argv[]);
       argv是需要的参数的一个字符串数组
       char *argv[]={"ps","aux",NULL;
       execv("usr/bin/ps",argv);
*/
#include <stdio.h>
#include <unistd.h>

int main() {
  //创建一个子进程，在子进程中执行exec函数族中的函数

  pid_t pid = fork();
  if (pid > 0) {
    printf(" i am parent  process,id:%d\n", getpid());
  } else {
    // execl("hello", "hello", NULL);
    // execlp("ps", "ps", "aux", NULL);
    char *argv[] = {"ps", "aux", NULL};
    execv("/usr/bin/ps", argv);
    perror("execv");
    printf(" i am child  process,id:%d", getpid());
  }
  for (int i = 0; i < 3; ++i) {
    printf("i=%d,pid=%d\n", i, getpid());
    sleep(1);
  }
}