/*
带v不带l的一类exac函数，包括execv、execvp、execve，应先构造一个指向各参数的指针数组，
然后将该数组的地址作为这些函数的参数。
*/
// 如char *arg[]这种形式，且arg最后一个元素必须是NULL，例如char *arg[] =
// {“ls”,”-l”,NULL}; 下面以execvp函数为例说明：
//文件execvp.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//函数原型：int execvp(const char *file, char *const argv[]);

int main(void) {
  printf("before execlp****\n");
  char *argv[] = {"ps", "-l", NULL};
  if (execvp("ps", argv) == -1) {
    printf("execvp failed!\n");
  }
  printf("after execlp*****\n");
  return 0;
}