/*
       #include <stdlib.h>
       void exit(int status);

      #include <unistd.h>
       void _exit(int status);

     status参数：是进程退出时的一个状态信息。父进程回收子进程资源的时候可以获取

*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main() {
  printf("hello\n");
  printf("world");
  // exit(0);//会刷新IO缓冲区、
  _exit(0);  //不会刷新IO缓冲区
  return 0;
}