/*
       #include <sys/types.h>
       #include <sys/wait.h>

       pid_t wait(int *wstatus);
           功能;等待任意一个子进程结束，如果任意一个子进程结束
           参数:  int   *wstatus
                进程退出时的状态信息，传入的是一个int类型的地址，传出参数
           返回值：
               --成功：返回被回收的子进程id
               --失败：-1   (所有的子进程都结束了)
    调用wait函数的进程会被挂起（阻塞），知道它的一个子进程退出或者收到一个不能被忽略的信号，
    这个时候才被唤醒（继续向下执行），如果没有紫禁城了，函数会立即返回，返回-1，如果子进程
    都已经结束了，也会立即返回
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main() {
  //有一个父进程，创建5个进程（兄弟）
  pid_t pid;
  for (int i = 0; i < 5; ++i) {
    pid = fork();
    if (pid == 0) {
      break;
    }
  }
  if (pid > 0) {
    while (1) {
      printf("parent id:%d\n", getpid());
      // int ret = wait(NULL);  //这个时候用kill -9  子进程号  就能被回收了
      int st;
      int ret = wait(&st);
      if (ret == -1) {  //因为主进程有回收机制
        break;
      }
      if (WIFEXITED(st)) {
        //正常退出
        printf("退出的状态码:%d\n", WEXITSTATUS(st));
      } else if (WIFSIGNALED(st)) {
        //信号退出
        printf("被哪个信号干掉了的：%d", WTERMSIG(st));
      }
      printf("child die,pid=%d\n", ret);
      sleep(1);
    }
  } else {
    while (1) {
      printf("child id:%d\n", getpid());
      sleep(1);
      // exit(1);
    }
  }
}