/*
       #include <sys/types.h>
       #include <sys/wait.h>
       pid_t waitpid(pid_t pid, int *wstatus, int options);
  功能：回收指定进程号的子进程，可以设置是否阻塞
        -pid:
        pid>0:  某个子进程的pid
        pid=0:  回收当前进程组的所有子进程
        pid=-1:回收任意一个子进程，相当于wait()
        pid<-1: 回收某个进程组的组id的绝对值，回收指定进程组的子进程
       options:
       0阻塞
       WNOHANG  非阻塞的。
       -返回值：
       >0：返回子进程的id
       =0(非阻塞的情况下):options=WNOHANG,表示还有子进程或者
       =-1：没有子进程了
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main() {
  //有一个父进程，创建5个进程（兄弟）
  pid_t pid;
  for (int i = 0; i < 5; ++i) {
    pid = fork();
    if (pid == 0) {
      break;
    }
  }
  if (pid > 0) {
    while (1) {
      printf("parent id:%d\n", getpid());
      // int ret = wait(NULL);  //这个时候用kill -9  子进程号  就能被回收了
      int st;
      // int ret = waitpid(-1, &st, 0);
      int ret = waitpid(-1, &st, WNOHANG);  //非阻塞
      if (ret == -1) {                      //因为主进程有回收机制
        break;
      }
      if (ret == 0) {
        sleep(1);
        continue;
      } else if (ret > 0) {
        if (WIFEXITED(st)) {
          //正常退出
          printf("退出的状态码:%d\n", WEXITSTATUS(st));
        } else if (WIFSIGNALED(st)) {
          //信号退出
          printf("被哪个信号干掉了的：%d", WTERMSIG(st));
        }
        printf("child die,pid=%d\n", ret);
      }
      sleep(1);
    }
  } else {
    while (1) {
      printf("child id:%d\n", getpid());
      sleep(1);
      // exit(1);
    }
  }
}