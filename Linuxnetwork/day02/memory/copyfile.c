
//使用内存映射实现文件拷贝的功能(一般不能拷贝太大的文件)
/*

    思路：
      1.对原始的文件进行内存映射
      2.创建一个新文件
      3.把新文件的数据映射到内存中
      4.通过内存拷贝将第一个文件的内存数据拷贝到新的文件内存中
      5.释放资源
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main() {
  int fd = open("test.txt", O_RDWR);
  int size = lseek(fd, 0, SEEK_END);
  void *ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (ptr == MAP_FAILED) {
    perror("mmap");
    exit(0);
  }
  int fd1 = open("copy.txt", O_RDWR | O_CREAT, 0664);
  truncate("copy.txt", size);
  // write(fd, " ", 1);
  void *ptr1 = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd1, 0);
  if (ptr == MAP_FAILED) {
    perror("mmap");
    exit(0);
  }
  //内存拷贝
  memcpy(ptr1, ptr, size);

  // strncpy(ptr1, ptr, size);
  close(fd1);  //后进先出
  close(fd);
}
