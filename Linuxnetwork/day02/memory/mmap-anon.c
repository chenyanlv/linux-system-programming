/*
匿名映射：不需要文件实体进程一个内存映射
不能做没有关系的进程之间的映射。

*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
int main() {
  // 1.创建匿名内存映射区
  int len = 4096;
  void *ptr = mmap(NULL, len, PROT_READ | PROT_WRITE,
                   MAP_SHARED | MAP_ANONYMOUS, -1, len);  //
  if (ptr == MAP_FAILED) {
    perror("mmap");
    exit(0);
  }

  pid_t pid = fork();

  if (pid > 0) {
    wait(NULL);
    printf("%s", (char *)ptr);
  } else if (pid == 0) {
    strcpy(ptr, "ni hao a !!!");
  } else {
    perror("fork");
  }
  int ret = munmap(ptr, len);
  if (ret == -1) {
    perror("munmap");
    exit(0);
  }
}