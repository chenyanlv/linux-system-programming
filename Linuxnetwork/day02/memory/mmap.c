/*
课后学习知识：分页
       #include <sys/mman.h>

       void *mmap(void *addr, size_t length, int prot, int flags,
                  int fd, off_t offset);
         -功能：将一个文件或者设备数据映射到内存中
         -参数：
              -void *addr:NULL，由内核决定
              -length:要映射的数据的长度，这个值不能为0，建议使用文件的长度
                      读取文件的长度:stat lseek
              -prot:对申请的内存映射区的操作权限
                    PROT_EXEC  可执行的权限.
                    PROT_READ  读权限.
                    PROT_WRITE 写权限.
                    PROT_NONE  没有权限.
                    要操作映射内存，必须要有读的权限。
                    PROT_READ、PROT_READ|PROT_WRITE
              -flags:
             -MAP_SHARED:映射区的数据会自动和磁盘文件进行同步，进程间通信，必须要设置
             -MAP_PRIVATE:不同步，内存影深灰色去的数据改变了，对原来的文件不修改，而是在写的时候自己拷贝一个
              -fd:需要映射的那个文件的文件描述符
                  -通过open得到。open的是一个磁盘文件。
                  -注意：文件的大小不能为，open指定的权限不能和prot参数有冲突。
                  prot:PROT_READ    open:只读/读写
                prot：PROT_READ|PROT_WRITE   open:读写
                //prot的权限必须要小于等于open的权限
                -offset:偏移量，一般不用。必须指定的是4k的整数倍。0 表示不偏移
                  返回值：
                   成功:返回创建的内存的首地址
                   失败返回MAP_FATLED  (void*)  -1


       int munmap(void *addr, size_t length);
              -功能：释放内存映射
              -参数：
                  -addr：要释放的内存的首地址
                  -length:要释放的内存的大小，要和mmap函数中的length参数的值一样
*/
/*
使用内存映射实现进程通讯。
1.有关系的进程（父子进程）
    -还没有子进程的时候
         -通过唯一的父进程，先创建内存映射区
    -有了内存映射区以后，创建子进程
    -父子进程共享创建的内存映射区
2.没有关系的进程间通信
    -准备一个大小不是0的磁盘文件
    -进程1通过磁盘文件创建内存映射区
        -得到一个操作这块内存的指针
    -进程2通过磁盘文件创建这块内存到的指针
        -得到一个操作这块内存的指针
    -使用内存映射区通信

    注意：内存映射区通信，是非阻塞。

*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
int main() {
  // 1.打开一个文件
  //  void *mmap(void *addr, size_t length, int prot, int flags,
  //             int fd, off_t offset);
  int fd = open("hello.txt", O_RDWR);
  if (fd == -1) {
    perror("open");
  }
  int size = lseek(fd, 0, SEEK_END);
  void *ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  pid_t pid = fork();
  if (pid > 0) {
    strcpy((char *)ptr, "ni hao,son!!!");
  } else if (pid == 0) {
    sleep(2);
    char buf[64];
    strcpy(buf, (char *)ptr);
    printf("read data: %s\n", buf);
  } else {
    perror("fork");
  }
  munmap(ptr, size);
  wait(NULL);
  return 0;
}