#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
int main() {
  // 1.打开一个文件
  //  void *mmap(void *addr, size_t length, int prot, int flags,
  //             int fd, off_t offset);
  int fd = open("hello.txt", O_RDWR);
  if (fd == -1) {
    perror("open");
  }
  int size = lseek(fd, 0, SEEK_END);
  void *ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  char buf[64];
  while (1) {
    for (int i = 0; i < 20; ++i) {
      sprintf(buf, "%s 第%d次", "ni hao a", i);
      strcpy((char *)ptr, buf);
      printf("send data:%s\n", buf);
      sleep(2);
    }
  }
  // sleep(2);
  // char buf[64];
  // strcpy(buf, (char *)ptr);
  // printf("read data: %s\n", buf);
  munmap(ptr, size);
  return 0;
}