/*
B 端
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main() {
  //创建管道 FIFO1  FIFO2
  int ret = access("fifo1", F_OK);
  if (ret == -1) {
    printf("管道文件不存在,创建管道\n");
    ret = mkfifo("fifo1", 0664);
    if (ret == -1) {
      perror("mkfifo");
      exit(0);
    }
  }
  ret = access("fifo2", F_OK);
  if (ret == -1) {
    printf("管道文件不存在,创建管道\n");
    ret = mkfifo("fifo2", 0664);
    if (ret == -1) {
      perror("mkfifo");
      exit(0);
    }
  }
  int fdr = open("fifo1", O_RDONLY);
  if (fdr == -1) {
    perror("open");
    exit(0);
  }
  int fdw = open("fifo2", O_WRONLY);
  if (fdw == -1) {
    perror("open");
    exit(0);
  }
  char buf[1024];
  pid_t pid = fork();
  if (pid > 0) {
    //主进程写

    printf("发送端启动成功\n");
    sleep(1);
    while (1) {
      memset(buf, 0, 1024);
      fgets(buf, 1024, stdin);
      ret = write(fdw, buf, strlen(buf));
      if (ret == -1) {
        perror("write");
        break;
      }
    }
  } else if (pid == 0) {
    //子进程读
    printf("接收端启动成功\n");
    sleep(1);
    while (1) {
      memset(buf, 0, 1024);
      ret = read(fdr, buf, 1024);
      if (ret <= 0) {
        perror("read");
        printf("发送端退出/读取失败\n");
        break;
      }
      printf("A对你说:%s\n", buf);
    }

  } else {
    perror("fork");
  }
  close(fdw);
  close(fdr);
  wait(NULL);
}