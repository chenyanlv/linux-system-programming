/*
创建fifo文件
1.通过命令 mkfifo 名字 创建
2.通过函数 int mkfifo(const char *pathname, mode_t mode);创建
       #include <sys/stat.h>
       #include <sys/types.h>

       int mkfifo(const char *pathname, mode_t mode);
       参数：
       -pathname:管道名称的路径
       -mode:文件的权限  mode &~umask  八进制的数
       返回值：成功返回0
               失败返回-1  并设置 errno
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  int ret = mkfifo("fifo01", 0664);
  if (ret == -1) {
    perror("nkfifo");
    exit(0);
  }
}