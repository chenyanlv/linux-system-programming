/*
       #include <unistd.h>

       long fpathconf(int fd, int name);



*/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
int main() {
  int pipefd[2];
  int ret = pipe(pipefd);
  long size = fpathconf(pipefd[0], _PC_PIPE_BUF);
  printf("pipe size: %ld\n", size);
}
