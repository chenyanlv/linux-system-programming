/*
实现ps aux|grep xxx父子进程间通信

子进程：ps aux,子进程结束后，将数据发给父进程
父进程：获取到数据过滤，过滤。
pipe()
execlp()
子进程将标准输出stdout_fileno重定向到管道
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main() {
  int pipefd[2];
  pipe(pipefd);
  pid_t pid = fork();
  if (pid > 0) {
    close(pipefd[1]);  //关闭写端
    int len = -1;
    char buf[1024] = {0};
    while ((len = read(pipefd[0], buf, sizeof(buf) - 1)) > 0) {
      printf("%s", buf);
      memset(buf, 0, 1024);
    }
    wait(NULL);
  } else if (pid == 0) {
    close(pipefd[0]);                //关闭读端
    dup2(pipefd[1], STDOUT_FILENO);  //将输出的文件描述符重定向到写端
    execlp("ps", "ps", "aux", NULL);
    perror("execlp");
  } else {
    perror("fork");
  }
}