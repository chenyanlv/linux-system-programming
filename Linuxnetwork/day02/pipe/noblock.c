/*
  设置管道非阻塞
   int flags=fcntl(fd[0],F_GETFL);
   flags|=O|NONBLOCK;
   fcntl(fd[0],F_SETFL,flags);
*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
//子进程发送数据给父进程，父进程读取数据输出
int main() {
  //在fork之前创建管道
  int pipefd[2];
  int ret = pipe(pipefd);
  if (ret == -1) {
    perror("pipe");
    exit(0);
  }
  //创建子进程
  pid_t pid = fork();
  if (pid > 0) {
    //父进程
    //从管道的读取端读取数据
    close(pipefd[1]);  //关闭写端
    char buf[1024] = {0};
    printf("i am parent process, pid : %d\n", getpid());
    int flags = fcntl(pipefd[0], F_GETFL);
    flags |= O_NONBLOCK;
    fcntl(pipefd[0], F_SETFL, flags);
    while (1) {
      int len = read(pipefd[0], buf, sizeof(buf));  //阻塞的去读  有数据才读
      printf("len:%d\n", len);
      printf("parent recv:%s,pid:%d\n", buf, getpid());
      bzero(buf, 1024);
      // char *str = "hello i am parent";
      // write(pipefd[1], str, strlen(str));
      sleep(1);
    }

  } else if (pid == 0) {
    //子进程
    printf("i am child process, pid : %d\n", getpid());
    char buf[1024] = {0};
    while (1) {
      close(pipefd[0]);  //关闭读端
      char *str = "hello i am child";
      write(pipefd[1], str, strlen(str));
      sleep(5);
      // bzero(buf, 1024);
      // int len = read(pipefd[0], buf, sizeof(buf));  //阻塞的去读  有数据才读
      // printf("child recv:%s,pid:%d\n", buf, getpid());
    }
  } else {
    perror("fork");
    exit(0);
  }
  return 0;
}