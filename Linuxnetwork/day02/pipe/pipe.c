/*
    #include <unistd.h>

  int pipe(int pipefd[2]);
     功能:创建一个匿名管道，用来进程间通信。
     参数： int pipedfd[2]  这是一个传出参数
                pipefd[0]         读端
                pipefd[1]        写端
     返回值：  成功返回 0
               失败返回-1

    注意  匿名管道只能用于具有关系的进程之间的通信
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
//子进程发送数据给父进程，父进程读取数据输出
int main() {
  //在fork之前创建管道
  int pipefd[2];
  int ret = pipe(pipefd);
  if (ret == -1) {
    perror("pipe");
    exit(0);
  }
  //创建子进程
  pid_t pid = fork();
  if (pid > 0) {
    //父进程
    //从管道的读取端读取数据
    char buf[1024] = {0};
    printf("i am parent process, pid : %d\n", getpid());
    while (1) {
      int len = read(pipefd[0], buf, sizeof(buf));  //阻塞的去读  有数据才读
      printf("parent recv:%s,pid:%d\n", buf, getpid());

      char *str = "hello i am parent";
      write(pipefd[1], str, strlen(str));
      sleep(1);
      bzero(buf, 1024);
    }

  } else if (pid == 0) {
    //子进程
    printf("i am child process, pid : %d\n", getpid());
    char buf[1024] = {0};
    while (1) {
      char *str = "hello i am child";
      write(pipefd[1], str, strlen(str));
      sleep(1);
      bzero(buf, 1024);
      int len = read(pipefd[0], buf, sizeof(buf));  //阻塞的去读  有数据才读
      printf("child recv:%s,pid:%d\n", buf, getpid());
    }
  } else {
    perror("fork");
    exit(0);
  }
  return 0;
}