#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  //打开管道
  int fd = open("test", O_RDONLY);
  if (fd == -1) {
    perror("open");
  }
  char buf[1024] = {0};
  while (1) {
    int len = read(fd, buf, 1024);
    if (len == 0) {
      break;
    }
    printf("%s\n", buf);
    memset(buf, 0, 1024);
    sleep(1);
  }
}