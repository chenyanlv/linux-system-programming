/*

   有名管道的注意事项
     1.一个为只读而打开一个管道的进程会阻塞，直到另一个进程以写权限的打开管道
     1.一个为只写而打开一个管道的进程会阻塞，直到另一个进程以读权限的打开管道
  读管道：
  管道中如有数据，read返回实际读到的字节数
  管道中无数据：
      管道写端被全部关闭，read返回0，（相当于读到文件末尾）
      写端没有全部被关闭，read阻塞等待

   写管道：
      管道读端被全部关闭 ，进程异常终止
      （这种情况一般是，先被读端唤醒后，读端又全部关闭，才会收到一个SIGPIPE信号）
      //如果是一开始读端还没打开，那么也会阻塞到open函数
      管道读端没有被全部关闭：
        管道已经满了，write 会阻塞
        管道没有满，write会写入，并返回实际写入的字节数。
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  //管道文件不存在，创建有名管道

  int ret = access("test", F_OK);
  if (ret == -1) {
    printf("管道不存在，创建管道");
    mkfifo("test", 0664);
  }
  //打开管道
  int fd = open("test", O_WRONLY);
  // printf("start write data\n");
  while (1) {
    char *buf = "hello i am write process";
    write(fd, buf, strlen(buf));
    printf("write data is:%s\n", buf);
    sleep(1);
  }
}
