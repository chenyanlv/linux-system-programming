/*
       #include <sys/types.h>
       #include <unistd.h>

       pid_t fork(void);
       函数的作用：用于创建子进程
       返回值：
            fork()的返回值会返回两次。一次是在父进程中，一次是在子进程中。
            1.在父进程中返回创建的子进程的ID，
            2.在子进程中返回0
            如何区分父进程和子进程：通过fork的返回值
            在父进程中返回-1，标识创建子进程失败，并且设置errno
      失败的两个原因：
      1.当前系统的进程数已经达到了系统规定的上限，这时errno的值被设置为EAGAIN
      2.系统内存不足，这时errno的值被设置为ENOMEM


      父子进程之间的关系:
      区别：
      1.fork()函数的返回值是不同的
         父进程:>0 返回的子进程的ID
         子进程中:=0;
      2.pcb中 的一些数据
         当前的进程的id pid
         当前的进程的父进程的id ppid
         信号集
      共同点：
      某些状态下：子进程刚被创建出来，还没有执行任何任何的写数据的操作
          -用户去的数据
          -文件描述符表
      父子进程对变量是不是共享的？
          -刚开始到的时候，是一样的，共享的。如果修改了数据，不共享了
          -读时共享，写时拷贝
*/
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main() {
  //创建子进程
  pid_t pid = fork();
  if (pid == 0) {
    pid_t pid1 = getpid();
    printf("标识id：%d\n", pid);
    printf("子进程id：%d\n", pid1);
    for (int i = 0; i < 10; ++i) {
      printf("i:%d\n", i);
      sleep(1);
    }
  } else if (pid < 0) {
    perror("fork");
  } else {
    pid_t pid1 = getpid();
    printf("子进程id:%d\n", pid);
    printf("父进程id:%d\n", pid1);
    for (int i = 0; i < 10; ++i) {
      printf("j:%d\n", i);
      sleep(1);
    }
    // fflush(stdout);
    //     fflush(stdin)刷新标准输入缓冲区，把输入缓冲区里的东西丢弃[非标准]
    // fflush(stdout)刷新标准输出缓冲区，把输出缓冲区里的东西打印到标准输出设备上
  }
}