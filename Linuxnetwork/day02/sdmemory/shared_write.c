#include <stdio.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
int main() {
  //创建一个共享内存
  int shmid = shmget(100, 4096, IPC_CREAT | 0664);
  //和当前进程进程关联
  void *ptr = shmat(shmid, NULL, 0);
  char *str = "helloworld";
  //写数据
  memcpy(ptr, str, strlen(str) + 1);
  printf("请按任意键继续\n");
  getchar();
  shmdt(ptr);
  shmctl(shmid, IPC_RMID, NULL);
  return 0;
}