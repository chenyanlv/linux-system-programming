#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main() {
  char *buf;
  strcpy(buf, "hello");
  // printf("%s", buf);
  return 0;
}
// strcpy 操作申请了内存的指针
/*
core 文件可能会受注释的影响，若果要使用core调试
尽量将注释跟代码同行放置
*/
// char *buf = (char *)(malloc(sizeof(char)));
/*
ulimit -c 1024
ulimit -a

查看core文件生产目录
cat /proc/sys/kernel/core_pattern

使用下面的命令
sudo bash -c "echo core > /proc/sys/kernel/core_pattern "
*/
