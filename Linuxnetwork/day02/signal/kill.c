/*

int kill(pid_t pid,int sig);
     -功能：给某个进程pid，发送某个信号 sig
     -参数：
        -pid:需要发送给的进程的id
          >0发送给指定pid的进程
          =0发送给当前进程组的进程
          -1:（每一个有权限接收这个信号的进程）
          <-1 发送给这个数绝对值的进程组的进程
        -sig:需要发送的信号的编号或者是宏值,0表示不发送任何信合
        //推荐用宏，因为不同架构下的编号可能不一样
           kill(getdppid(),9)//示例
           kill(getpid(),9)

int raise(int sig);
    -功能：给当前进程发送信号
    -参数:
        -sig：要发送的信号
    -返回值：
        -成功 0
        -失败:非0

void abort(void);
-功能：发送SIGABRT信号给当前的进程，杀死当前进程
kill(getpid(),SIGABRT);
*/
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  pid_t pid = fork();

  if (pid == 0) {
    for (int i = 0; i < 5; i++) {
      printf("child process\n");
      sleep(1);
    }
  } else {
    sleep(2);
    printf("kill child process\n");
    kill(pid, SIGINT);
  }
}