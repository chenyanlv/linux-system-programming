/*
周期性的定时
   #include <sys/time.h>

 int setitimer(int which, const struct itimerval *new_value,
                     struct itimerval *old_value);
- 功能设置定时器(闹钟)。可以替代alarm函数。精度比alarm要高,精度微秒us 、
而alarm的精度只有s。可以实现周期性定时(周期性的发送某个信号)。
-参数:
  which:定时器以什么时间定时
    -ITIMER_REAL    真实时间（时间到达，发送信号SIGALRM） 常用
    -ITIMER_VIRTUAL 用户时间(时间到达，信号SIGVTALRM)
    -ITIMER_PROF    用户时间+内核时间(时间到达，信号SIGPROF) 以该进程在用户态和
                    内核态下所消耗的时间来计算
  new_value:设置定时器的属性

struct itimerval {//设置定时器的属性
struct timeval it_interval; //每个阶段的时间
struct timeval it_value;  // 延迟多少时间执行定时器
}
;

struct timeval {        //表示时间的结构体
  time_t tv_sec;        // 秒
  suseconds_t tv_usec;  // 微秒
};


  过10秒之后，每隔两秒定时一次
  -old_value:记录上一次定时的时间参数  如果不用，可以传递一个NULL

  -返回值：
       成功返回0，
       失败返回-1，并设置errno的值

       为了周期性的定时，要捕捉到信号
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
int main() {
  //过3秒以后，每隔两秒钟定时一次
  struct itimerval new_value;

  //设置间隔的时间
  new_value.it_interval.tv_sec = 2;
  new_value.it_interval.tv_usec = 0;
  //设置延迟的时间3秒，第一次发送的信号延迟3秒
  new_value.it_value.tv_sec = 3;
  new_value.it_value.tv_usec = 0;
  int ret = setitimer(ITIMER_REAL, &new_value, NULL);  //非阻塞的。
  printf("定时器开始了\n");
  if (ret == -1) {
    perror("setitimer");
    exit(0);
  }
  getchar();
  return 0;
}