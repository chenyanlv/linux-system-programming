#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
void myalarm(int num) { printf("捕捉到了信号的编号是：%d\n", num); }
int main() {
  struct sigaction act;
  // act.sa_flags = 0;
  // sigaction中的sa_flags未设置SA_RESTART所以会是第二种行为，系统调用被中断，
  act.sa_flags = SA_RESTART;
  act.sa_handler = myalarm;
  sigemptyset(&act.sa_mask);  //清空临时阻塞信号集

  //注册信号捕捉
  sigaction(SIGALRM, &act, NULL);

  //过3秒以后，每隔两秒钟定时一次
  struct itimerval new_value;

  //设置间隔的时间
  new_value.it_interval.tv_sec = 2;
  new_value.it_interval.tv_usec = 0;
  //设置延迟的时间3秒，第一次发送的信号延迟3秒
  new_value.it_value.tv_sec = 3;
  new_value.it_value.tv_usec = 0;
  int ret = setitimer(ITIMER_REAL, &new_value, NULL);  //非阻塞的。
  printf("定时器开始了\n");
  if (ret == -1) {
    perror("setitimer");
    exit(0);
  }
  // while (1) {
  // }
  getchar();  // getchar会被sigaction函数终端，所以用getchar阻塞进程会出问题
  perror("getchar");
  return 0;
}
