/*
SIGCHLD信号产生的3个条件
1.子进程结束
2.子进程暂停了
3.子进程继续运行
都会给父进程发送该信号。父进程默认是忽略该信号

使用SIGCHLD解决僵尸进程的问题
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
//这里需要注意的是，需要考虑未决信号的特点
//当未决信号在执行时，如果这是内核向主进程发送了相同的信号，那么这个信号是会被丢弃的
void myFun(int num)
{
  printf("收到信号：%d\n", num);
  int ret = 0;
  while (1)
  {
    ret = waitpid(-1, NULL, WNOHANG); //设置为非阻塞
    if (ret > 0)
    {
      printf("child die,pid=%d\n", ret);
    }
    else if (ret == 0)
    {
      //说明还有子进程或者
      break;
    }
    else
    {
      //没有子进程
      break;
    }
  }
}
int main()
{
  //为了解决子进程可能在信号捕捉函数没初始化完的时候就结束，提前设置阻塞信号集
  // sigset_t set;
  // sigemptyset(&set.sa_mask);
  // sigaddset(&set, SIGCHLD);
  // sigprocmask(SIG_BLOCK, &set, NULL);

  pid_t pid;

  for (int i = 0; i < 20; ++i)
  {
    pid = fork();
    if (pid == 0)
    {
      break;
    }
  }
  if (pid > 0)
  {
    struct sigaction act;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    act.sa_handler = myFun;
    sigaction(SIGCHLD, &act, NULL);
    // sigprocmask(SIG_UNBLOCK, &set, NULL);
    while (1)
    {
      printf("parent process is running,pid:%d\n", getpid());
      sleep(2);
    }
  }
  else if (pid == 0)
  {
    printf("child process,pid:%d\n", getpid());
  }
  else
  {
    perror("fork");
  }
}
//当没有阻塞该信号时，可能会会出现段错误
/*
视频中出现段错误的原因在于在信号处理函数中调用了不可重入的函数：
使用gdb调试跟踪函数调用栈
最下层f 23可以看到是在main函数中，再往上f 22是在父进程中调用了printf
再往上f
10可以看到调用了信号处理函数，这里是我们在main函数中调用printf但是printf还没有调用完成，直接转到了信号处理函数，我这里的信号处理函数为handler，见f
9，再往上f 8调用printf，可以看到f 8 和f 22是一样的
SIGSEGV是因为printf会分配临时空间，在主函数调用printf至malloc时，中断处理函数调用，在其中也调用了printf至malloc时就出现了错误。
*/