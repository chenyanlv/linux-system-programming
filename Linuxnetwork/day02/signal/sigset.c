/*
 以下函数是对自定义的信号集进行操作

int sigemptyset(sigset_t *set);
   -功能：清空信号集中的数据，将信号集中的所有标志位置为0
   -参数：set，传出参数，需要操作的信号集
   -返回值：成功返回0，失败返回-1



int sigfillset(sigset_t *set);
    -功能：将信号集中的所有的标志位置为1
    -参数：set，传出参数，需要操作的信号集
    -返回值：成功返回0，失败返回-1.


int sigaddset(sigset_t *set,int signum);
     -功能：设置信号集中的某一信号对应的标志位置为1，表示阻塞这个信号
    -参数：
       -set，传出参数，需要操作的信号集
       -signum：需要阻塞的那个信号
    -返回值：成功返回0，失败返回-1.


int sigdelset(sigset_t *set,int signum);
  -功能：设置信号集中的某一信号对应的标志位置为0，表示不阻塞这个信号
    -参数：
       -set，传出参数，需要操作的信号集
       -signum：需要阻塞的那个信号
    -返回值：成功返回0，失败返回-1.

int sigismember(const sigset_t *set,int signum);
  -功能：判断某个信号是否阻塞
  -参数：
      -set：需要操作的信号集
      -signum:需要判断的信号
      -返回值：
        1:表示signum被阻塞
        0:signum不阻塞


int sigprocmask(int how,const sigset_t *set,sigset_t *oldset);
int sigpending(sigset_t *set);
*/
#include <signal.h>
#include <stdio.h>
int main() {
  //创建一个信号集
  sigset_t set;

  //清空信号集的内容

  sigemptyset(&set);
  //判断SIGINT 是否在信号集内

  int ret = sigismember(&set, SIGINT);
  if (ret == 0) {
    printf("SIGINT 不阻塞\n");
  } else if (ret == 1) {
    printf("SIGINT 阻塞\n");
  }

  //添加几个信号到信号集中
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGQUIT);
  ret = sigismember(&set, SIGINT);
  if (ret == 0) {
    printf("SIGINT 不阻塞\n");
  } else if (ret == 1) {
    printf("SIGINT 阻塞\n");
  }
  ret = sigismember(&set, SIGQUIT);
  if (ret == 0) {
    printf("SIGQUIT 不阻塞\n");
  } else if (ret == 1) {
    printf("SIGQUIT 阻塞\n");
  }
  return 0;
}
