/*
int pthread_cond_init(pthread_cond_t *restrict cond,constpthread_condattr_t
*restrict attr); int pthread_cond_destroy(pthread_cond_t *cond);

int pthread_cond_wait (pthread_cond_t
*restrictcond,pthread_mutex_t*restrictmutex);
     -阻塞函数  调用了该函数，线程会阻塞。
int pthread_cond_timedwait (pthread_cond_t *restrict cond, pthread_mutex_t
*restrict mutex,const struct timespec *restrictabstime);
     -等待多长时间，调用了这个函数，线程会阻塞，直到指定的时间结束


int pthread_cond_signal(pthread_cond_t *cond) ;
     -唤醒一个或者多个等待的线程

int pthread_cond_broadcast(pthread_cond_t *cond);
     -唤醒所有的等待的线程。

*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define true 1;
#define false 0;
typedef int bool;
pthread_mutex_t mutex;
pthread_cond_t customer;
pthread_cond_t producer;

typedef struct queue
{
  int size;
  int *que;
  int head;
  int tail;
} queue;
queue workque;
void queue_init(queue *que, int size)
{
  que->size = size;
  que->que = (int *)malloc(sizeof(int) * (size + 1)); //循环队列会预留一个空位
  int head = 0;
  int tail = 0;
}

bool empty(queue *que)
{
  if (que->head == que->tail)
  {
    return true;
  }

  return false;
}

bool full(queue *que)
{
  return (que->tail + 1) % que->size == que->head;
}

void *work_customer(void *arg)
{
  while (1)
  {
    pthread_mutex_lock(&mutex);
    while (empty(&workque))
    {
      pthread_cond_wait(&customer, &mutex);
    }
    printf("tid:%ld, customer thread  take node val:%d\n ", pthread_self(), workque.que[workque.head]);
    workque.head = (workque.head + 1) % workque.size;
    pthread_cond_signal(&producer);
    pthread_mutex_unlock(&mutex);
    // sleep(1);
  }
  return NULL;
}
void *work_producer(void *arg)
{
  while (1)
  {
    pthread_mutex_lock(&mutex);
    while (full(&workque))
    {
      pthread_cond_wait(&producer, &mutex);
    }
    workque.que[workque.tail] = rand() % 1000 + 1;
    printf("tid:%ld, producer thread  produce node val:%d\n ", pthread_self(), workque.que[workque.tail]);
    workque.tail = (workque.tail + 1) % workque.size;
    pthread_cond_signal(&customer);
    pthread_mutex_unlock(&mutex);
    // sleep(1);
  }
  return NULL;
}

void work()
{
  int size;
  printf("请输入你的工作队列的大小:\n");
  scanf("%d", &size);
  queue_init(&workque, size);
  //  时间初始化种子
  time((time_t *)(NULL));
  //初始化互斥锁   初始化 条件变量
  pthread_mutex_init(&mutex, NULL);
  pthread_cond_init(&customer, NULL);
  pthread_cond_init(&producer, NULL);
  //创建消费者线程  生产者线程
  pthread_t ctid[5];
  pthread_t ptid[5];
  for (int i = 0; i < 5; ++i)
  {
    pthread_create(&ctid[i], NULL, work_customer, NULL);
    pthread_create(&ptid[i], NULL, work_producer, NULL);
  }

  //连接线程等待线程结束，回收资源
  for (int i = 0; i < 5; ++i)
  {
    pthread_join(ctid[i], NULL);
    pthread_join(ptid[i], NULL);
  }

  //释放信号量 和 互斥量（锁）
  pthread_mutex_destroy(&mutex);
  pthread_cond_destroy(&customer);
  pthread_cond_destroy(&producer);

  //释放队列中申请的内存
  free(workque.que);
}
int main()
{
  work();
  return 0;
}