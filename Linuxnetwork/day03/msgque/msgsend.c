/*
1.创建或打开消息队列函数  //打开一个现存队列或者创建一个新队列
int msgget(key_t key, int msgflg);
-参数
  -key 获取队列标识符的key  由ftok函数产生
  -msgflag
 如果msgflg同时指定了IPC_CREAT和IPC_EXCL，并且key的消息队列已经存在，那么
 msgget()在errno设置为EEXIST时失败。(这类似于组合的效果 O_CREAT | O_EXCL for open(2).)


-返回值
  成功 返回消息队的标识符（非负的值）
  失败返回-1 并且设置errno的相应错误号

2.添加消息函数
int msgsnd(int msgid, const void *msg_ptr, size_t msg_sz, int msgflg);
参数：
msg_sz：一般为发送的消息长度
msg_ptr:
    一般位结构体，并且是消息类型加消息的结构体形式。
    可以指定类型值，然后，读取的时候依据类型读取

-返回值：
 成功，消息数据的一分副本将被放到消息队列中，并返回0，失败时返回-1.

3.从一个消息队列中获取消息:
//接收者可以先进先出的接收消息，也可以通过修改 msgtype的值去改变接收顺序


int msgrcv(int msgid, void *msg_ptr, size_t msg_st, long int msgtype, int msgflg);
-参数
  msgid：消息队列特定的标识
  msg_ptr:消息结构 可以由用户自定义，但接受端和发送端需要同步
  msgflg可以指定IPC_NOWAIT 为非阻塞
  msgtype:
    0:返回队列中的第一个消息
    msgtype大于0:返回队列中消息类型为msgtype的第一个消息
    msgtype小于0:返回队列中消息类型值小于或等于msgtype绝对值，
    而且在这种消息中其类型值又小的消息。

-返回值：
调用成功时，该函数返回放到接收缓存区中的字节数，消息被复制到由msg_ptr指向的用户分配的缓存区中，
然后删除消息队列中的对应消息。失败时返回-1.


4.消息队列控制函数
int msgctl(int msgid, int command, struct msgid_ds *buf);
-参数：
command是将要采取的动作，它可以取3个值，

IPC_STAT：把msgid_ds结构中的数据设置为消息队列的当前关联值，即用消息队列的当前关联值覆盖msgid_ds的值。

IPC_SET：如果进程有足够的权限，就把消息列队的当前关联值设置为msgid_ds结构中给出的值

IPC_RMID：删除消息队列
buf是指向msgid_ds结构的指针，它指向消息队列模式和访问权限的结构。msgid_ds结构至少包括以下成员：
struct msgid_ds { uid_t shm_perm.uid; uid_t shm_perm.gid; mode_t shm_perm.mode; };

-返回值:
成功时返回0，失败时返回-1.



*/
#include <stdio.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define BUFFER_SIZE 512
struct message
{
  long msg_type;
  char msg_text[BUFFER_SIZE];
};
int main()
{
  int qid;
  key_t key;
  struct message msg;
  if ((key = ftok(".", 'a')) == -1)
  {
    perror("ftok");
    exit(1);
  }
  if ((qid = msgget(key, IPC_CREAT | 0666)) == -1)
  {
    perror("msgget");
    exit(1);
  }
  printf("open queue %d\n", qid);
  while (1)
  {
    printf("Enter some message to the queue:\n");
    if ((fgets(msg.msg_text, BUFFER_SIZE, stdin)) == NULL)
    {
      puts("no message");
      exit(1);
    }
    msg.msg_type = getpid();
    if ((msgsnd(qid, &msg, strlen(msg.msg_text), 0)) < 0)
    {
      perror("message posted");
      exit(1);
    }
    if (strncmp(msg.msg_text, "quit", 4) == 0)
    {
      break;
    }
  }
  exit(0);
}
