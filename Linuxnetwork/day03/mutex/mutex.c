/*
互斥量的类型  pthread_mutex_t

int pthread_mutex_init(pthread_mutex_t *restrict mutex,const pthread_mutexattr_t
*restrict attr);

-初始化 -初始化互斥量 -参数： -mutex:需要初始化的互斥量变量
     -attr:互斥量相关的属性，NULL
    -restrict:C语言的修饰符，被修饰的指针，不能由另外一个指针进行操作。
   pthread_mutexattr_t *restrict attr=a;
   pthread_mutexattr_t *restrict attr1=attr;


int pthread_mutex_destroy(pthread_mutex_t *mutex);
    -释放互斥量的资源


int pthread_mutex_lock(pthread_mutex_t *mutex);
    -加锁，阻塞的，如果有一个线程加锁了，那么其他的线程只能阻塞等待


int pthread_mutex_trylock (pthread_mutex_t *mutex) ;
     -尝试加锁，如果加锁失败，不会阻塞，会直接返回。


int pthread mutex_unlock(pthread mutex_t *mutex) ;
     -解锁




*/

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
pthread_mutex_t mutex;
int ticket = 100;
void *takeTicket(void *arg) {
  int ret;
  while (1) {
    // pthread_mutex_lock(&mutex);
    ret = pthread_mutex_lock(&mutex);
    if (ret != 0) {
      char *errstr = strerror(ret);
      printf("%s\n", errstr);
      continue;
    } else {
      printf("trylock success\n");
    }
    if (ticket > 0) {
      usleep(60000);
      printf("thread id:%ld,is taking  %d ticket\n", pthread_self(), ticket);
      --ticket;
    } else {
      pthread_mutex_unlock(&mutex);
      break;
    }
    pthread_mutex_unlock(&mutex);
  }
  return NULL;
}
int main() {
  pthread_t tid[3];
  for (int i = 0; i < 3; ++i) {
    pthread_create(&tid[i], NULL, takeTicket, NULL);
  }

  for (int j = 0; j < 3; ++j) {
    pthread_join(tid[j], NULL);
  }
  pthread_mutex_destroy(&mutex);
  return 0;
}