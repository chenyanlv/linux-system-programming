/*
读写锁的类型pthread_rwlock_t
int pthread_rwlock_init(pthread_rwlock_t *restrict rwlock,const
pthread_rwlockattr t *restrict attr);

int pthread_rwlock_destroy(pthread_rwlock_t *rwlock);
int pthread_rwlock_rdlock (pthread_rwlock_t *rwlock);
int pthread_rwlock_tryrdlock(pthread_rwlock_t *rwlock);
int pthread_rwlock_wrlock (pthread_rwlock_t *rwlock) ;
int pthread_rwlock_trywrlock(pthread_rwlock_t *rwlock);
int pthread rwlock unlock(PTHREAD_rwlock_t *rwlock) ;

案例：8个线程操作同一个全局变量
3个线程不定时写这个全局变量，5个线程不定时的读这个全局变量



*/
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
// pthread_mutex_t mutex;
pthread_rwlock_t rwlock;
int num = 1;
void *writeNum(void *arg) {
  while (1) {
    // pthread_mutex_lock(&mutex)
    pthread_rwlock_wrlock(&rwlock);
    ++num;
    printf("++write,tid:%ld,num:%d\n", pthread_self(), num);
    // pthread_mutex_unlock(&mutex);
    pthread_rwlock_unlock(&rwlock);
    usleep(3000);
  }
  return NULL;
}
void *readNum(void *arg) {
  while (1) {
    // pthread_mutex_lock(&mutex);
    pthread_rwlock_rdlock(&rwlock);
    printf("==read,tid:%ld,num:%d\n", pthread_self(), num);
    // pthread_mutex_unlock(&mutex);
    pthread_rwlock_unlock(&rwlock);
    usleep(3000);
  }
  return NULL;
}
int main() {
  //创建三个写线程，5个读线程
  // pthread_mutex_init(&mutex, NULL);
  pthread_rwlock_init(&rwlock, NULL);
  pthread_t wtids[3], rtids[5];
  for (int i = 0; i < 3; ++i) {
    pthread_create(&wtids[i], NULL, readNum, NULL);
  }
  for (int i = 0; i < 5; ++i) {
    pthread_create(&rtids[i], NULL, writeNum, NULL);
  }
  for (int i = 0; i < 3; ++i) {
    pthread_join(wtids[i], NULL);
  }
  for (int i = 0; i < 5; ++i) {
    pthread_join(rtids[i], NULL);
  }
  // pthread_mutex_destroy(&mutex);
  pthread_rwlock_destroy(&rwlock);
  return 0;
}