/*
信号量的类型sem_t
int sem_init (sem_t *sem，int pshared,unsigned int value);
-初始化信号量
-参数：
    sem:信号量
    phsared: 0用在线程，非0用在进程
    -value:信号量中的值

int sem_destroy (sem_t *sem) ;
    -释放资源

int sem_wait (sem_t *sem) ;
    -对信号量加锁，调用一次对信量的值-1,如果值为0，就阻塞

int sem_trywait (sem_t *sem);

int sem_timedwait(sem_t *sem，const struct timespec *abs_timeout);

int sem_post ( sem_t * sem) ;
   -对信号量解锁，调用一次对信号量的值+1

int sem_getvalue ( sem_t * sem,int * sval) ;
   -

*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#define true 1;
#define false 0;
typedef int bool;
pthread_mutex_t mutex;
sem_t customer;
sem_t producer;

typedef struct queue
{
  int size;
  int *que;
  int head;
  int tail;
} queue;
queue workque;
void queue_init(queue *que, int size)
{
  que->size = size;
  que->que = (int *)malloc(sizeof(int) * (size + 1)); //循环队列会预留一个空位
  que->head = 0;
  que->tail = 0;
}

bool empty(queue *que)
{
  if (que->head == que->tail)
  {
    return true;
  }

  return false;
}

bool full(queue *que)
{
  return (que->tail + 1) % que->size == que->head;
}

void *work_customer(void *arg)
{
  while (1)
  {
    pthread_mutex_lock(&mutex);
    sem_wait(&customer);
    printf("tid:%ld, customer thread  take node val:%d\n ", pthread_self(), workque.que[workque.head]);
    workque.head = (workque.head + 1) % workque.size;
    sem_post(&producer);
    pthread_mutex_unlock(&mutex);
    // sleep(1);
  }
  return NULL;
}

void *work_producer(void *arg)
{
  while (1)
  {
    pthread_mutex_lock(&mutex);
    sem_wait(&producer);
    workque.que[workque.tail] = rand() % 1000 + 1;
    printf("tid:%ld, producer thread  produce node val:%d\n ", pthread_self(), workque.que[workque.tail]);
    workque.tail = (workque.tail + 1) % workque.size;
    sem_post(&customer);
    pthread_mutex_unlock(&mutex);
    // sleep(1);
  }
  return NULL;
}

void work()
{
  int size;
  printf("请输入你的工作队列的大小:\n");
  scanf("%d", &size);
  queue_init(&workque, size);
  //  时间初始化种子
  time((time_t *)(NULL));
  //初始化互斥锁   初始化 条件变量
  pthread_mutex_init(&mutex, NULL);
  sem_init(&customer, 0, 0);
  sem_init(&producer, 0, size);
  //创建消费者线程  生产者线程
  pthread_t ctid[5];
  pthread_t ptid[5];
  for (int i = 0; i < 5; ++i)
  {
    pthread_create(&ctid[i], NULL, work_customer, NULL);
    pthread_create(&ptid[i], NULL, work_producer, NULL);
  }

  //连接线程等待线程结束，回收资源
  for (int i = 0; i < 5; ++i)
  {
    pthread_join(ctid[i], NULL);
    pthread_join(ptid[i], NULL);
  }

  //释放信号量 和 互斥量（锁）
  pthread_mutex_destroy(&mutex);

  sem_destroy(&customer);
  sem_destroy(&producer);

  //释放队列中申请的内存
  free(workque.que);
}
int main()
{
  work();
  return 0;
}