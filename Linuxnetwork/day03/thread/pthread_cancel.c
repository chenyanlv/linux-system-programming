/*
         #include <pthread.h>

       int pthread_cancel(pthread_t thread);
                -功能：取消线程（让线程终止，前提是线程没有不能取消的属性）
                    取消某个线程，可以终止某个线程的运行。
                    但是不是立马终止，而是当子线程执行到一个取消点,线程才会终止
                    取消点：系统规定的一些系统调用，我们可以粗略的理解为从用户区到内核区的切换，这个位置，称之为取消点
*/
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
void *callback(void *arg) {
  printf("child thread id:%ld", pthread_self());
  for (int i = 0; i < 5; ++i) {
    printf("child:%d\n", i);
  }

  return NULL;
}
int main() {
  //创建一个子线程
  pthread_t tid;

  int ret = pthread_create(&tid, NULL, callback, NULL);
  if (ret != 0) {
    char *errorstr = strerror(ret);
    printf("%s\n", errorstr);
  }
  //取消线程

  pthread_cancel(tid);

  for (int i = 0; i < 5; ++i) {
    printf("parent:%d\n", i);
  }
  //输出主线程和子线程的id
  printf("child:%ld,parent%ld\n", tid, pthread_self());
  pthread_exit(NULL);
  return 0;
}
