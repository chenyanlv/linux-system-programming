/*
       一般情况下，main函数所在的线程我们称之为主线程(main线程)
       其创建的称之为子线程
       程序中默认只有一个进程，fork()函数调用，2进行
       程序中只有一个线程，pthread_create()函数，两个线程。


       #include <pthread.h>

       int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg);
            -功能：创建一个子线程
            -参数：
              -thread:传出参数，线程创建成功后，子线程的线程ID被写到该变量中
              -attr:设置线程的属性，一般使用默认值，NULL
              -start_routine:函数指针，这个函数是子线程需要处理的逻辑代码
              -arg:给第三个参数使用，传参
              -返回值:
                   成功返回0，失败会返回错误号，和之前的errno不一样，不能通过perror输出
                   获取错误的信息：char strerror(int errnum);
*/
#include <pthread.h>
#include <stdio.h>
#include <string.h>
void *callback(void *arg) {
  printf("child thread\n");
  return NULL;
}
int main() {
  //创建一个子线程
  pthread_t tid;

  int ret = pthread_create(&tid, NULL, callback, NULL);
  if (ret != 0) {
    char *errstr = strerror(ret);
    printf("%s\n", errstr);
  }
  pthread_join(tid, NULL);
}