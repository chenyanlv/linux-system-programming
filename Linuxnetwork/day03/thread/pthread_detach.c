/*
       #include <pthread.h>
       int pthread_detach(pthread_t thread);
      -功能:分离一个线程。被分离的线程在终止的时候，会自动释放资源返回给系统。
      1.不能多次分离，会产生不可预料的行为
      2.不能去连接一个已经分离的线程，会报错。
      -参数;需要分离的线程的id
      -返回值：
        成功：返回0
        失败：返回错误号

*/
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
void *callback(void *arg) {
  int seconds = 0;
  while (1) {
    printf("thread child\n");
    sleep(1);
    if (seconds == 6) {
      break;
    }
    ++seconds;
  }
  return NULL;
}
int main() {
  //创建一个子线程
  pthread_t tid;

  int ret = pthread_create(&tid, NULL, callback, NULL);
  if (ret != 0) {
    char *errorstr = strerror(ret);
    printf("%s\n", errorstr);
  }
  //输出主线程和子线程的id
  printf("child:%ld,parent%ld\n", tid, pthread_self());
  //设置子线程分离，子线程分离后，子线程的资源不需要主线程回收
  //这里需要注意，如果子线程已经设置了和主线程分离，那就不能再和主线程连接了(调用pthread_join())
  pthread_detach(tid);
  printf("parent thread die\n");
  pthread_exit(NULL);
  return 0;
}
