/*
     #include <pthread.h>
       void pthread_exit(void *retval);
        功能：终止一个线程，在哪个线程中调用，就表示终止哪个线程
        参数：
            retval:需要传递一个指针，作为一=一个返回值，可以在pthread_join()函数当中获取到

       pthread_t pthread_self(void);
         功能:获取当前的线程的ID。

        int pthread_equal(pthread_t t1, pthread_t t2);
          功能：比较两个线程号是否相等
          不同的操作系统，pthread_t的类型实现是不一样的，有的是无符号的长整形，有的是结构体是实现的
*/
#include <pthread.h>
#include <stdio.h>
#include <string.h>
void *callback(void *arg) {
  printf("child process id:%ld\n", pthread_serlf());

  return NULL;
}
int main() {
  //创建一个子线程
  pthread_t tid;
  int ret = pthread_create(&tid, NULL, callback, NULL);
  if (ret != 0) {
    char *errstr = strerror(ret);
    printf("%s\n", errstr);
  }
  for (int i = 0; i < 6; ++i) {
    printf("%d\n", i);
  }
  printf("rid:%ld,main thread id:%ld\n", tid, pthread_self());
  //让主线程退出，是不影响其他线程的
  pthread_exit(NULL);
  printf("main thread exit \n");
  return 0;
}