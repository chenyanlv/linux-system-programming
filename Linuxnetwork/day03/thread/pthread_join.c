/*
      #include <pthread.h>

       int pthread_join(pthread_t thread, void **retval);
            -功能：和一个已经终止的线程进行连接
           任何一个线程都可以去回收线程，不过一般由主线程回收
              这个函数时阻塞函数,调用一次只能回收一个子线程
           -参数：
           -thread:需要回收的子线程的ID
           -retval（二级指针）:接收子线程退出时的返回值
           -返回值：
            0： 成功返回
            非0：失败，返回的错误号

            那么这里为什么要传递二级指针呢?
  函数callback返回的是一个指针，要想接收这个返回值需要一个指针类型。
所以定义了 int *thread_retval去接收返回的指针。
但是如果直接将
thread_retval传入函数pthread_join，它就是一个局部参数，当结束函数pthread_join回到主线程，thread_retval的值是不会改变的。这样起不到用thread_retval接收返回值的作用。
要改变值，就要传递地址，所以得传&thread_retval，因为thread_retval是一个指针，而&是取地址，所以就成了**的参数类型。
*/

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
int value = 10;
void *callback(void *arg) {
  printf("child process id:%ld\n", pthread_self());
  sleep(3);
  // int value = 10;                //这样传，栈内存会被回收
  pthread_exit((void *)&value);  //等同于 (void *)&value;
  // return NULL;
}
int main() {
  //创建一个子线程
  pthread_t tid;
  int ret = pthread_create(&tid, NULL, callback, NULL);
  if (ret != 0) {
    char *errstr = strerror(ret);
    printf("%s\n", errstr);
  }
  for (int i = 0; i < 6; ++i) {
    printf("%d\n", i);
  }
  //主线程调用pthread_join()回收子线程。
  void *thread_retval;
  pthread_join(tid, &thread_retval);
  printf("exit data:%d", *(int *)thread_retval);
  printf("回收子线程资源成功\n");
  printf("rid:%ld,main thread id:%ld\n", tid, pthread_self());
  //让主线程退出，是不影响其他线程的
  pthread_exit(NULL);
  return 0;
}