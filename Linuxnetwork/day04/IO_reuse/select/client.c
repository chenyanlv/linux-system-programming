#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main()
{

  //创建用于通信的socket
  int sfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sfd == -1)
  {
    perror("socket");
  }
  //连接服务端
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &(saddr.sin_addr.s_addr));
  saddr.sin_port = htons(9999);
  int ret = connect(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
  if (ret == -1)
  {
    perror("connect");
  }
  int num = 0;
  char buf[1024] = {0};
  while (1)
  {
    sprintf(buf, "send data %d\n", num++);
    ret = write(sfd, buf, strlen(buf) + 1);
    if (ret == 0)
    {
      printf("sever close ,.....quit\n");
      close(sfd);
      break;
    }
    else if (ret == -1)
    {
      perror("write");
    }
    ret = read(sfd, buf, sizeof(buf));
    if (ret > 0)
    {
      printf("data from server:%s", buf);
    }
    else if (ret == 0)
    {
      printf("sever close ,.....quit\n");
      close(sfd);
      break;
    }
    else if (ret == -1)
    {
      perror("read");
    }
    sleep(1);
  }
}