#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>

void work()
{
  //创建监听的socket

  int lfd = socket(AF_INET, SOCK_STREAM, 0);
  if (lfd == -1)
  {
    perror("socket");
    exit(-1);
  }
  //绑定  地址和端口
  struct sockaddr_in saddr;
  saddr.sin_port = htons(9999);
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &(saddr.sin_addr.s_addr));

  int ret = bind(lfd, (struct sockaddr *)(&saddr), sizeof(saddr));

  if (ret == -1)
  {
    perror("bind");
    exit(-1);
  }

  ret = listen(lfd, 4096);
  printf("bind success start listen\n");
  if (ret == -1)
  {
    perror("listen");
    exit(-1);
  }

  //现在的size  底层已经不再使用了 所以任意一个大于0的数

  int epfd = epoll_create(100);

  struct epoll_event epollev;
  epollev.events = EPOLLIN;
  epollev.data.fd = lfd;
  struct epoll_event epollarr[1024];
  epoll_ctl(epfd, EPOLL_CTL_ADD, lfd, &epollev);
  char buf[1024] = {0};
  while (1)
  {
    int ret = epoll_wait(epfd, epollarr, sizeof(epollarr), -1);

    if (ret > 0)
    {
      for (int i = 0; i < ret; ++i)
      {
        int curfd = epollarr[i].data.fd;
        if (curfd == lfd)
        {
          printf("ret:%d\n", ret);
          struct sockaddr_in cliaddr;
          int len = sizeof(cliaddr);
          int cfd = accept(lfd, (struct sockaddr *)&cliaddr, &len);
          epollev.events = EPOLLIN;
          epollev.data.fd = cfd;
          epoll_ctl(epfd, EPOLL_CTL_ADD, cfd, &epollev);
          printf("fd:%d ,client connect\n", cfd);
        }
        else
        {
          if (epollarr[i].events & EPOLLOUT)
          {
            continue;
          }

          if (epollarr[i].events & EPOLLIN)
          {
            int len = read(curfd, buf, sizeof(buf));

            if (len == -1)
            {
              perror("read");
              exit(-1);
            }
            else if (len == 0)
            {
              printf("client closed...\n");
              epoll_ctl(epfd, EPOLL_CTL_DEL, curfd, NULL);
              close(curfd);
            }
            else if (len > 0)
            {
              printf("read buf = %s\n", buf);
              write(curfd, buf, strlen(buf) + 1);
            }
          }
        }
      }
    }
    else if (ret == -1)
    {
      perror("epollwait");
      exit(-1);
    }
  }
  close(epfd);
  close(lfd);
}

int main()
{

  work();
}