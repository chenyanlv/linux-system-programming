#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/socket.h>
int main()
{
  //创建监听的socket
  int lfd = socket(AF_INET, SOCK_STREAM, 0);

  //主机字节序转为网络字节序
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &(saddr.sin_addr.s_addr));
  saddr.sin_port = htons(9999);
  //绑定地址和端口
  bind(lfd, (struct sockaddr *)&(saddr), sizeof(saddr));

  //开始监听
  listen(lfd, 1020);
  printf("绑定成功，开始监听\n");
  fd_set o_set, temp;
  FD_ZERO(&o_set);
  FD_SET(lfd, &o_set);
  int maxfd = lfd;
  while (1)
  {
    temp = o_set;

    int ret = select(maxfd + 1, &temp, NULL, NULL, NULL);
    if (ret == -1)
    {
      perror("select");
      exit(-1);
    }
    if (FD_ISSET(lfd, &temp))
    {
      struct sockaddr_in caddr;
      int len;
      int cfd = accept(lfd, (struct sockaddr *)&caddr, &len);
      FD_SET(cfd, &o_set);

      char ip_str[16];
      inet_ntop(AF_INET, &caddr.sin_addr, ip_str, 16);
      caddr.sin_port = ntohs(caddr.sin_port);
      printf("has client connect addr:%s,port:%d\n", ip_str, caddr.sin_port);
      maxfd = cfd > maxfd ? cfd : maxfd;
    }
    char buf[1024];
    for (int i = lfd + 1; i <= maxfd; ++i)
    {
      if (FD_ISSET(i, &temp))
      {
        int len = read(i, buf, sizeof(buf));
        if (len > 0)
        {
          printf("fd:%d,msg:%s", i, buf);
          write(i, buf, strlen(buf) + 1);
        }
        else if (len == 0)
        {
          printf("fd:%d close\n", i);
          close(i);
          FD_CLR(i, &o_set);
        }
        else if (len == -1)
        {
          perror("read");
          exit(-1);
        }
      }
    }
  }
  close(lfd);
}