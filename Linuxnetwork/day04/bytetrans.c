/*

#include <arpa/inet.h>
//端口转换
uint16_t htons(uint16_t hostshort);
uint16_t ntohs(uint16_t netshort);
//ip 转换
uint 32_t hton1(uint32_t hostlong);
uint 32_t ntoh1(uint32_t netlong);

*/
#include <stdio.h>
#include <arpa/inet.h>
int main()
{
  unsigned short a = 0x0102;
  unsigned short b = htons(a);
  printf("%x\n", b);
  unsigned char str_ip[4] = {192, 168, 90, 129};
  for (int i = 0; i < 4; ++i)
  {
    printf("%d ", *(str_ip + i));
  }
  printf("\n");
  int num = *(int *)str_ip;
  int nnum = htonl(num);
  unsigned char *n_ip = (char *)&nnum;
  printf("%d %d %d %d \n", *n_ip, *(n_ip + 1), *(n_ip + 2), *(n_ip + 3));
}