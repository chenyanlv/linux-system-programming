#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
  //创建用于通信的套接字
  int sfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sfd == -1)
  {
    perror("socket");
    exit(-1);
  }
  //设定服务器的地址和端口号
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &(saddr.sin_addr));
  saddr.sin_port = htons(9999);

  //连接获取服务区端的文件描述符号
  int ret = connect(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
  if (ret == -1)
  {
    perror("connect");
    exit(-1);
  }
  char buf[1024] = {0};
  while (1)
  {
    scanf("%s", buf);
    write(sfd, buf, sizeof(buf));
    int size = read(sfd, buf, sizeof(buf));
    if (size > 0)
    {
      printf("%s\n", buf);
    }
    else if (size == 0)
    {
      printf("服务端关闭连接\n");
      //服务端关闭连接
      break;
    }
    else
    {
      perror("read");
      exit(-1);
    }
  }
  close(sfd);
}