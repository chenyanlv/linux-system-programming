#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
  //创建 监听的 socket
  int lfd = socket(AF_INET, SOCK_STREAM, 0);
  if (lfd == -1)
  {
    perror("socket");
    exit(-1);
  }
  //绑定地址和端口
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &(saddr.sin_addr));
  saddr.sin_port = htons(9999);
  int ret = bind(lfd, (struct sockaddr *)&saddr, sizeof(saddr));
  if (ret == -1)
  {
    perror("bind");
    exit(-1);
  }
  //开始监听
  listen(lfd, 4096);
  //接收客户端的连接
  struct sockaddr_in caddr;
  int len = sizeof(caddr);
  int fd = accept(lfd, (struct sockaddr *)&caddr, &len);

  if (fd == -1)
  {
    perror("accept");
    exit(-1);
  }

  //将网络字节序转换为主机字节序
  char ip_addr[16];
  inet_ntop(AF_INET, &(caddr.sin_addr.s_addr), ip_addr, 16);
  unsigned short port = ntohs(caddr.sin_port);

  printf("有客户端连接上了\n");
  printf("ip:%s ,port:%d\n", ip_addr, port);
  char buf[1024] = {0};
  while (1)
  {
    int size = read(fd, buf, sizeof(buf));
    if (size > 0)
    {
      printf("%s\n", buf);
    }
    else if (size == 0)
    {
      //客户端关闭连接
      printf("客户端关闭连接\n");
      break;
    }
    else
    {
      perror("read");
      exit(-1);
    }
    scanf("%s", buf);
    write(fd, buf, sizeof(buf));
  }
  close(lfd);
  close(fd);
}