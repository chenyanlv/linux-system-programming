#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main()
{

  //创建通信的socket

  int sfd = socket(AF_INET, SOCK_DGRAM, 0);

  if (sfd == -1)
  {
    perror("socket");
    exit(-1);
  }

  struct sockaddr_in caddr;
  caddr.sin_family = AF_INET;
  caddr.sin_addr.s_addr = INADDR_ANY; //实际应该用广播地址更好
  // inet_pton(AF_INET, "192.168.90.255", &caddr.sin_addr.s_addr);
  caddr.sin_port = htons(9999);

  int ret = bind(sfd, (struct sockaddr *)&caddr, sizeof(caddr));
  //广播的时候  实际上服务器并不需要在绑定端口，这个时候，其实客户才需要绑定指定的端口以及地址
  if (ret == -1)
  {
    perror("bind");
    exit(-1);
  }
  //接收客户端传来的信息
  printf("start  recv broadcast\n");
  int len;
  char buf[1024] = {0};
  while (1)
  {
    int len = sizeof(caddr);
    printf("len:%d\n", len);
    recvfrom(sfd, buf, sizeof(buf), 0, (struct sockaddr *)&caddr, &len);
    printf("server say：%s\n", buf);
  }
}