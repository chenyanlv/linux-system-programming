/*
//设置广播属性的函数
int setsockopt(int sockfd,int level,int optname,const void *optval,socklen_t optlen);
    -sockfd:文件描述符
    -level:SOL_SOCKET
    -optname:SO_BROADCAST
    -optval:int 类型的值，为1表示允许广播
    -optlen:optval的大小
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main()
{

  //创建通信的socket

  int sfd = socket(AF_INET, SOCK_DGRAM, 0);

  if (sfd == -1)
  {
    perror("socket");
    exit(-1);
  }

  //设置广播的属性
  int op = 1;
  setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &op, sizeof(op));

  //创建一个广播地址
  struct sockaddr_in caddr;
  caddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.255", &caddr.sin_addr.s_addr);
  caddr.sin_port = htons(9999);
  // bind(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
  //广播的时候  实际上服务器并不需要在绑定端口，这个时候，其实客户才需要绑定指定的端口以及地址

  //接收客户端传来的信息
  printf("broadcast server start \n");
  int len;
  char buf[1024] = {0};
  int num = 0;
  while (1)
  {
    sprintf(buf, "hello,client....%d\n", num++);
    sendto(sfd, buf, strlen(buf) + 1, 0, (struct sockaddr *)&caddr, sizeof(caddr));
    printf("广播的数据：%s\n", buf);
    sleep(1);
  }
}