#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main()
{

  //创建通信的socket

  int cfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (cfd == -1)
  {
    perror("scoket");
    exit(-1);
  }
  //绑定ip地址  端口

  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &saddr.sin_addr.s_addr);
  saddr.sin_port = htons(9999);

  //接收客户端传来的信息

  int len;
  char buf[1024] = {0};
  int ret;
  int num = 0;
  while (1)
  {
    sprintf(buf, "data:%d\n", num++);

    ret = sendto(cfd, buf, strlen(buf) + 1, 0, (struct sockaddr *)&saddr, sizeof(saddr));
    if (ret == -1)
    {
      perror("sendto");
      exit(-1);
    }

    len = recvfrom(cfd, buf, sizeof(buf), 0, NULL, NULL);
    if (len == -1)
    {
      perror("revcfrom");
      exit(-1);
    }
    if (len > 0)
    {
      printf("msg from server:%s", buf);
    }
    sleep(1);
  }
}