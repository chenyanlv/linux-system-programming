#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main()
{

  //创建通信的socket

  int sfd = socket(AF_INET, SOCK_DGRAM, 0);

  if (sfd == -1)
  {
    perror("socket");
    exit(-1);
  }
  //绑定ip地址  端口

  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.90.129", &saddr.sin_addr.s_addr);
  saddr.sin_port = htons(9999);
  bind(sfd, (struct sockaddr *)&saddr, sizeof(saddr));

  //接收客户端传来的信息
  printf("bind success  wait client connect\n");
  int len;
  char buf[1024] = {0};
  while (1)
  {
    char ip_str[16] = {0};
    struct sockaddr_in caddr;
    len = sizeof(caddr);
    len = recvfrom(sfd, buf, sizeof(buf), 0, (struct sockaddr *)&caddr, &len);
    if (len == -1)
    {
      perror("recvfrom");
      exit(-1);
    }
    printf("client ip:%s,port:%d", inet_ntop(AF_INET, &caddr.sin_addr.s_addr, ip_str, 16), ntohs(caddr.sin_port));
    printf("msg:%s", buf);
    sendto(sfd, buf, strlen(buf) + 1, 0, (struct sockaddr *)&caddr, sizeof(caddr));
  }
}