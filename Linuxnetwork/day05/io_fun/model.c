/*
       ssize_t readv(int fd, const struct iovec *iov, int iovcnt);

       ssize_t writev(int fd, const struct iovec *iov, int iovcnt);
          --参数说明：
           struct iovec {
               void  *iov_base;    // Starting address
               size_t iov_len;     // Number of bytes to transfer
            i o v数组中的元素数由i o v c n t说明。
}


        --return value
       On success, readv(), preadv() and preadv2() return the number of bytes read; writev(), pwritev()
       and pwritev2() return the number of bytes written.

       Note that it is not an error for a successful call to transfer fewer bytes than  requested  (see
       read(2) and write(2)).

       On error, -1 is returned, and errno is set appropriately.
;

*/